package com.example.cse438.studio6.fragment

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.provider.MediaStore
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.example.cse438.studio6.App
import com.example.cse438.studio6.R
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.fragment_create_account.*

@SuppressLint("ValidFragment")
class CreateAccountFragment(context: Context): Fragment() {

    private var parentContext = context
    val REQUEST_IMAGE_CAPTURE = 1
    val REQUEST_IMAGE_FROM_GALLERY = 2

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return layoutInflater.inflate(R.layout.fragment_create_account, container, false)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if ((requestCode == REQUEST_IMAGE_CAPTURE || requestCode == REQUEST_IMAGE_FROM_GALLERY) && resultCode == Activity.RESULT_OK) {
            profile_pic.setImageBitmap(
                if (requestCode == REQUEST_IMAGE_CAPTURE) {
                    data?.extras?.get("data") as? Bitmap
                } else {
                    val returnUri = data?.data
                    MediaStore.Images.Media.getBitmap(context!!.contentResolver, returnUri)
                }
            )
        }
    }

    override fun onStart() {
        super.onStart()

//        profile_pic.setOnClickListener {
//            val dialog = Dialog(parentContext)
//            dialog.setContentView(R.layout.dialog_camera_or_gallery)
//
//            val window = dialog.window
//            window?.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT)
//
//            dialog.findViewById<Button>(R.id.gallery).setOnClickListener {
//                val intent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
//                intent.type = "image/*"
//                dialog.dismiss()
//                startActivityForResult(intent, REQUEST_IMAGE_FROM_GALLERY)
//            }
//
//            dialog.findViewById<Button>(R.id.camera).setOnClickListener {
//                Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { it1 ->
//                    it1.resolveActivity(context!!.packageManager).also {
//                        dialog.dismiss()
//                        startActivityForResult(it1, REQUEST_IMAGE_CAPTURE)
//                    }
//                }
//            }
//
//            dialog.findViewById<Button>(R.id.close).setOnClickListener {
//                dialog.dismiss()
//            }
//
//            dialog.show()
//        }

        create_account.setOnClickListener {
            val firstName = first_name.text.toString()
            val lastName = last_name.text.toString()
            val email = email.text.toString()
            val username = username.text.toString()
            val password = password.text.toString()
            //val profilePic = App.convertDrawableToUri(parentContext.applicationContext, profile_pic.drawable, 150, 150)

            if (firstName != "" && lastName != "" && email != "" && username != "" && password != "") {
                App.firebaseAuth?.createUserWithEmailAndPassword(email, password)?.addOnCompleteListener {it2 ->
                    if (it2.isSuccessful) {
                        val db = FirebaseFirestore.getInstance()
                        val userData = HashMap<String, Any>()
                        userData["first_name"] = firstName
                        userData["last_name"] = lastName
                        userData["email"] = email
                        userData["username"] = username

                        db.document("users/${App.firebaseAuth?.currentUser?.uid}")
                            .set(userData)
                            .addOnSuccessListener {
                                activity?.finish()
                            }
                            .addOnFailureListener {
                                Toast.makeText(parentContext, "Failed to write user data", Toast.LENGTH_SHORT).show()
                            }
                    }
                    else {
                        Toast.makeText(parentContext, "Email and/or password unacceptable", Toast.LENGTH_SHORT).show()
                    }
                }
            }
            else {
                Toast.makeText(parentContext, "Must fill all fields", Toast.LENGTH_SHORT).show()
            }
        }
    }
}